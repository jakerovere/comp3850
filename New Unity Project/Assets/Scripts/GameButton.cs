﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameButton : MonoBehaviour
{
    private SceneController boss; // To manage calls to change the Scene
    public GameObject thisPanel;
    public GameObject nextPanel;
    public int function; /* Determines the function of this button.
                            1 - Changes Panel; 2 - Changes Scene; 3 - Toggle Button */
    public bool sceneChanger = false;
    public string nextSceneName = "";

    public bool toggleCorrect = false;
    public bool toggleIncorrect = false;
    public int toggleTotalCorrect = 0;
    public GameButton toggleFinalLocation = null;
    public bool toggleClicked = false;
    public Sprite toggleImage = null;

    private void Start()
    {
        boss = GameObject.FindObjectOfType<SceneController>();
    }

    public void ButtonPressed()
    {
        switch (function)
        {
            case 1:
                ChangePanel();
                break;
            case 2:
                ChangeScene();
                break;
            case 3:
                ToggleButton();
                break;
        }
    }

    public void ChangePanel()
    {
        nextPanel.SetActive(true);
        thisPanel.SetActive(false);
        boss.totalToggleGoal = 0;
        CheckCorrect();
    }

    public void ChangeScene()
    {
        SceneManager.LoadScene(nextSceneName, LoadSceneMode.Single);
    }

    public void ToggleButton()
    {
        this.GetComponent<Image>().sprite = toggleImage;
        if(toggleCorrect == true && toggleClicked == false)
        {
            boss.totalToggleGoal = toggleTotalCorrect;
            boss.totalToggles++;
            this.toggleClicked = true;
            if(boss.totalToggleGoal == boss.totalToggles)
            {
                toggleFinalLocation.function = 1;
            }
        }
        CheckCorrect();
    }

    public void CheckCorrect()
    {
        if(toggleIncorrect == true)
        {
            boss.SetScore(-20);
        }
        if(toggleCorrect == true)
        {
            boss.SetScore(20);
        }
    }
}


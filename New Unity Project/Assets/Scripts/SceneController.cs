﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class SceneController : MonoBehaviour
{
    public Canvas UICanvas;
    public GameObject activePanel;
    public GameObject menuPanel;
    public GameObject textPanel;
    public GameObject decision1;
    public GameObject decision2;
    public GameObject decision3;
    public Text scoreText;
    public Text objectiveText;

    public int totalToggleGoal = 0;
    public int totalToggles = 0;
    public GameObject toggleFinalLocation;

    private int score = 0;
    private bool paused = false;

    // Start is called before the first frame update
    void Start()
    {
        // Display text. You have to use 'GetComponentInChildren', because the text component
        // is a child of the Panel object.
        //textPanel.GetComponentInChildren<Text>().text = "You are a male police officer patrolling the festival with a male police officer partner. You have seen multiple teenagers going back and forth between the festival and a building.";

        //decision1.GetComponent<GameButton>().SetButtonProperties(2, "Approach Building", GameObject.Find("Approach Building").GetComponent<ScenarioEvent>());
        //decision2.GetComponentInChildren<Text>().text = "UNDER DEVELOPMENT";
        //decision3.GetComponentInChildren<Text>().text = "UNDER DEVELOPMENT";

        // Initialise score.
        scoreText.text = "Score: " + score;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) // Check if the game needs to be paused.
        {
            PauseGame();
        }
    }

    public void SetActivePanel(GameObject active)
    {
        activePanel = active;
    }

    public void PauseGame() // Pause the game, disable the currently active button panel and open the menu
    {
        if (paused == false)
        {
            paused = true;
            activePanel.SetActive(false);
            menuPanel.SetActive(true);
        } else
        {
            paused = false;
            activePanel.SetActive(true);
            menuPanel.SetActive(false);
        }
    }

    // Set the text displayed in the bottom panel.
    public void SetDisplayPanelText(string panelString)
    {
        textPanel.GetComponentInChildren<Text>().text = panelString;
    }

    // Set the value of score and update text.
    public void SetScore(int scoreBonus)
    {
        score += scoreBonus;
        scoreText.text = "Score: " + score;
    }

    public int GetScore() { return score; }

    // Set objectives.
    public void SetObjectives(string objectives)
    {
        objectiveText.text = objectives;
    }

}
